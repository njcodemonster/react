import React from 'react';
import logo from './logo.svg';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import './App.css';

var numberArr = [1,2,3,4,5,6,7,8,9,10];

function NumbersType( props ) {
    if(props.type == "odd"){
        return (<>{props.numberArr.map((item,i)=>{
            return (item%props.devisionBy != 0) ? <OrderList x={item} key={i+"for "+props.devisionBy}/> : '';
        })}</>)
    }else{
        return (<>{props.numberArr.map((item,i)=>{
            return (item%props.devisionBy == 0) ? <OrderList x={item} key={i+"for "+props.devisionBy}/> : '';
        })}</>)
    }
}

function OrderList(props){
    return <li>{props.x}</li>
}

function handleChange(props) {
    alert('A name was submitted: ' + props.number);
}

function Todo() {
    return (
        <div className="App">
            <header className="App-header">
                <img src={logo} className="App-logo" alt="logo" />
                <div>Input Box</div>
                <div><NumberInputForm /></div>
                <h3>Even Numbers:</h3>
                <ul><NumbersType numberArr={numberArr} devisionBy="2" /></ul>

                <h3>Odd Numbers:</h3>
                <ul><NumbersType numberArr={numberArr} devisionBy="2" type="odd"/></ul>

                <h3>Devision 3 Numbers:</h3>
                <ul><NumbersType numberArr={numberArr} devisionBy="3"/></ul>

                <h3>Devision 4 Numbers:</h3>
                <ul><NumbersType numberArr={numberArr} devisionBy="4"/></ul>

                <h3>Devision 5 Numbers:</h3>
                <ul><NumbersType numberArr={numberArr} devisionBy="5"/></ul>
            </header>
        </div>
    );
}

function NumberInputForm(){

    const InputNumber = React.useRef(null);

    const handleSubmit = e => {
        e.preventDefault();
        console.log(InputNumber.current.value);
        numberArr.push(parseInt(InputNumber.current.value));
        console.log(numberArr);
        InputNumber.current.value = '';
    };

    return (
        <form onSubmit={handleSubmit}>
            <label>Name:
                <input type="number" ref={InputNumber} />
            </label>
            <input type="submit" name="Submit" />
        </form>
    );
}

export default Todo;