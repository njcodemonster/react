import logo from './logo.svg';
import './App.css';
import React, { useState, useEffect ,useContext,createContext} from 'react';
const themes = {
  light: {
    foreground: "#000000",
    background: "#eeeeee"
  },
  dark: {
    foreground: "#ffffff",
    background: "#222222"
  }
};

const ThemeContext = React.createContext(null);

function App() {
  const [tth,setTth] = useState(themes.light)
  let toSend= {"val":tth,"setter":setTth}
  return (
    <ThemeContext.Provider value={toSend}>
      <Toolbar />
    </ThemeContext.Provider>
  );
}

function Toolbar(props) {
  return (
    <div>
      <ThemedButton />
    </div>
  );
}

function ThemedButton() {
  console.log(ThemeContext)
  const theme = useContext(ThemeContext);
  console.log(theme);
  return (
    <button style={{ background: theme.val.background}} onClick={()=>{theme.setter(themes.dark)}}>
      I am styled by theme context!
    </button>
  );
}
export default App;