import logo from './logo.svg';
import './App.css';
import React, { useState, useEffect } from 'react';
function contextUpdate() {
  return <h2>About</h2>;
}
function DemoC(props){
  return(
    <p>
         X Value is changed : {props.xvalue}
    </p>
  )
}
function ValueC(props){
  return <input type="txt" value={props.tvalue} onChange={(e) => props.handleTextChange(e,props.setter)} />
}
function App() {
  const [tvalue, setTvalue] = useState("");
  const [xvalue, setXvalue] = useState(0);
  const [yvalue, setYvalue] = useState(0);
  const [mixvalue, setMixValue] = useState(0);
  const [newT, setNewT] = useState(0);
  const handleTextChange=(e,setter)=>{
    e.preventDefault(e);
    setter(e.target.value);

  }
  useEffect(async() => {
    await setTvalue("FIRST TIME")
  },[]);

  useEffect(async () => {
    await setXvalue( tvalue.length);
  },[tvalue])

  useEffect(async () => {
   await setYvalue(xvalue.toString().length);
  },[xvalue])

  useEffect(async () => {
    await setMixValue(mixvalue+1);
  },[xvalue,yvalue])
  
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <ValueC tvalue={tvalue} handleTextChange={handleTextChange} setter={setTvalue}/>
        <ValueC tvalue={newT} handleTextChange={handleTextChange} setter={setNewT}/>
        <p>
         newT Value is changed : {newT}
        </p>
        <p>T The value is {tvalue}</p>
        <DemoC xvalue={xvalue} />
        <p>
         Y Value is changed : {yvalue}
        </p>
        <p>
         Mix Value is changed : {mixvalue}
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
